import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    sliderImage: [],
    arrivals: [],
    cart: []
  },
  mutations: {
    loadSliderImage(state, payload) {
      state.sliderImage = payload
    },
    loadArrivals(state, payload) {
      state.arrivals = payload
    },
    loadCart(state, payload) {
      state.cart.push(payload)
    }
  },
  getters: {
    getSliderImage: state => {
      return state.sliderImage;
    },
    getArrivals: state => {
      return state.arrivals;
    },
    getCart: state => {
      return state.cart;
    }
  },
  actions: {
    loadSliderImage(context) {
      context.commit('loadSliderImage', [
        {id: 1, src: require('@/assets/img/slider/slide_1.jpg'), alt: 'Slide 1'},
        {id: 2, src: require('@/assets/img/slider/slide_2.jpg'), alt: 'Slide 2'},
        {id: 3, src: require('@/assets/img/slider/slide_3.jpg'), alt: 'Slide 3'}
      ])
    },
    loadArrivals(context) {
      context.commit('loadArrivals', [
        {id: 1, src: require('@/assets/img/arrivals/photo1.jpg'), alt: 'Photo 1', title: 'Pastel summer dress', price: '1200', size: 'XS S M', code: '#535728'},
        {id: 2, src: require('@/assets/img/arrivals/photo2.jpg'), alt: 'Photo 2', title: 'leather shoes', price: '1200', size: 'XS S M', code: '#535728'},
        {id: 3, src: require('@/assets/img/arrivals/photo3.jpg'), alt: 'Photo 3', title: 'grey trench', price: '1200', size: 'XS S M', code: '#535728'},
        {id: 4, src: require('@/assets/img/arrivals/photo4.jpg'), alt: 'Photo 4', title: 'Minimal accessorie', price: '540', size: 'XS S M', code: '#535728'}
      ])
    },
    loadCart(context, product) {
      context.commit('loadCart', product)
    }
  }
})
